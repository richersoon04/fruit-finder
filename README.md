# README #

You can access the application by http://localhost:8080/FruitFinder/fruit.html?name=b
where localhost:8080 is the server url
FruitFinder is the name of the project
fruit.html is the view and the view has parameter of 'a' to 'z'

### What is this repository for? ###
Spring MVC Demo


* Quick summary
* Version
1.0

### How do I get set up? ###

This is a maven project with archetype for spring mvc

This is for spring mvc, so no actual database connection. We're just mocking up the database by creating hardcoded collections in java

How to run tests
http://localhost:8080/FruitFinder/fruit.html?name=a

### Who do I talk to? ###

* Repo owner or admin
Ramon Lansangan Jr
Email add: richersoon04@gmail.com