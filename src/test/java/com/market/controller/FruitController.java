package com.market.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.market.repositories.FruitRepository;

@Controller
public class FruitController {
	@Autowired
	FruitRepository fruitRepository;
	
	
	@RequestMapping(value="/fruit")
	public String findFruitByLetter(@RequestParam String name, Model model) {
		//retrieve the data from repository
		//then put data into the model, this can retrieve in view
		model.addAttribute("fruit", fruitRepository.findFruitByLetter(name));
		
		//return string is the name of the jsp
		return "fruit";
	}
}
