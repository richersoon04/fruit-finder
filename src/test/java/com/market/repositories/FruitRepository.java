package com.market.repositories;

import java.util.HashMap;

import org.springframework.stereotype.Repository;

import com.market.model.Fruit;

@Repository("fruitRepository")
public class FruitRepository {

	public Fruit findFruitByLetter(String name) {
		populateTestMap();
		return findFruitInMockDatabase(name);
	}

	//this is just a mock database
	//ideal database is connected using Hibernate or other ORM tool
	private Fruit findFruitInMockDatabase(String name){	
		for (String key : fruitMap.keySet()) {
			if(key.equalsIgnoreCase(name)) {
				Fruit fruit = new Fruit();
				fruit.setName(key);
				fruit.setFruit(fruitMap.get(key));
				return fruit;
			}
		}

		return null;
	}
	
	private HashMap<String, String> fruitMap = new HashMap<String, String>();
	
	private void populateTestMap(){
		fruitMap.put("A", "Apple");
		fruitMap.put("B", "Banana");
		fruitMap.put("C", "Carrot");
		fruitMap.put("D", "Daemon");
		fruitMap.put("E", "Egg Plant");
		fruitMap.put("F", "Fig");
		fruitMap.put("G", "Grapefruit");
		fruitMap.put("H", "Hardclause");
		fruitMap.put("I", "Italian punley");
		fruitMap.put("J", "Jackfruit");
		fruitMap.put("K", "Kiwi");
		fruitMap.put("L", "Lemon");
		fruitMap.put("M", "Mushroom");
		fruitMap.put("N", "Neutarise");
		fruitMap.put("O", "Olive");
		fruitMap.put("P", "Pumpkin");
		fruitMap.put("Q", "Quinee");
		fruitMap.put("R", "Radish");
		fruitMap.put("S", "Strawberry");
		fruitMap.put("T", "Tomato");
		fruitMap.put("U", "Ugli fruit");
		fruitMap.put("V", "Vadalian");
		fruitMap.put("W", "Walnut");
		fruitMap.put("X", "Xigua");
		fruitMap.put("Y", "Yellow pepper");
		fruitMap.put("Z", "Zocchina");
	}
}
